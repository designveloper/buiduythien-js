const CTA = document.querySelector(".cta a");
const ALERT = document.querySelector("#booking-alert");

CTA.classList.remove("hide");
ALERT.classList.add("hide");

function reveal(e, current) {

	// prevent default behavior of event object
    e.preventDefault();

    // toggle hide class
    current.innerHTML == "Book Now!"? CTA.innerHTML = "Ooops" : CTA.innerHTML = "Book Now!";
    ALERT.classList.toggle("hide");
}

CTA.addEventListener('click', function(e){reveal(e, this)} , false);
CTA.addEventListener('click', function(){console.log("The button was clicked!")}, false);
