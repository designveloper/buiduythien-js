##1. Javascript: An Introduction

- Javascript is a scripting language run in the browser and interacts with HTML markup and CSS Rules to change what you see and what you can do 

- jQuery

	+ Libraby of JS functions (write less, do more)
	
	+ CSS-like syntax, visual and UI enhancements

- JS Frameworks
	
	+ AngularJS, React, Vue.js, ...

	+ Front-end app frameworks used to simplify the building of advanced interactive web-based app

- Node.js
	
	+ JS platform built on browser runtime environments

	+ Used to run advanced operations and apps on servers and computers

##2. The Basics:

- JS loading methods

	+ Immediate: Causes JS render blocking

	+ Asynchronous: Add async attribute. Render blocking only happens when the script is executed

	+ Deferred: Add defer attribue. Only executes the script after everything's been loaded

- How to write good JS

	+ JS is case sensitive
	
	+ Use camelCase
	
	+ Whitespace matters

	+ End each statement with a semicolon

	+ Use comments liberally

##3. Working with data:

- Should use var prefix to create variables to avoid global declaration

- Data types in Javascript:

	+ Numeric: regular numbers and integers
	
	+ String: strng of letters and symbols
	
	+ Boolean
	
	+ null: store the intentional absence of a value
	
	+ undefined
	
	+ Symbol

- Arithmetic operators and math:

- Conditional statement and logic
	
	+ ==: More lenient
	
	+ ===: Absolute strict. Identical comparation (same data type)

- Array reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

##4. Functions and Objects:

- 3 types of functions:
	
	+ Named functions
	
	+ Anonymous functions

	+ Immediately invoked function expressions

- Const and Let
	
	+ const:
		
		+ Constant
		
		+ Can't be changed once defined
		
	+ let:
		
		+ Block scope variable
		
		+ Smaller scope than var

- Closures:
	
	+ A function inside a function, but relies on variables from the outside function to work
	
	+ Reference: https://developer.mozilla.org/en/docs/Web/JavaScript/Closures

##5. Javascript and the DOM, Part1: Changing DOM Elements

- DOM (document object model)
	
	+ is the model of the document that forms the current webpage
	
	+ modelling the relationships between the different nodes
	
- DOM methods:
	
	+ document.getElementById("some-ID")
	
	+ document.getElementsByClassName("class-name")

	+ document.geElementsByTagName("HTML tag")

	+ document.querySelector(".main-nav a") - Get the first element matching specified selector

	+ document.querySelectorAll(".post-content p") - Get all elements matching specified selector

- Access and change element:  innerHTML, outer HTML

- Access and change classes:

	+ .classlist.add/remove/item/toggle/contain/replace
	
	+ Reference: https://developer.mozilla.org/en/docs/Web/API/Element/classList
	
- Access and change attributes: .hasAttribute, .getAttribute, .setAttribute, removeAttribute

- Add DOM elements:

	+ Create the elements
	
	+ Create the text node that goes inside the element

	+ Add the text node to element

	+ Add the element to the DOM tree

- Add Inline CSS to an element:

	+ ...querySelector("selector").style.[CSS property]
	
	+ Overrides whatever CSS is applied to an element
	
	+ On the other hand, style is also an attribute -> The above methods for attribute can be used

	+ In most cases, best practice is to create CSS rules and use JS to manage these classes to apply the rules to the element

##6. Project: Create an Analog Clock

- <SVG></SVG>:
	
	+ SVG stands for Scalable Vector Graphics
	
	+  Used to define graphics for the Web
	
	+ SVG has several methods for drawing paths, boxes, circles, text, and graphic images.
	
- Transform property in CSS: This property allows you to rotate, scale, move, skew, etc., elements.

- Transition: To create a transition effect, you must specify two things:

	+ The CSS property you want to add an effect to
	
	+ The duration of the effect
	
##7. Javascript and the DOM, Part 2: Events

- Typical events:

	+ Common Browser-Level Events:
	
		+ Load: when the resource and dependents have finished loading
		
		+ Error: when a resource failed to load
		
		+ Online/Offline: seft-explanatory
		
		+ Resize: when a viewpoint is resized
		
		+ Scroll: when a viewpoint is srolled up/down/left/right
	
	+ Common DOM Events;
	
		+ Focus: when an element receives focus (clicked, tabbed to, etc.)
		
		+ Blur: when an element loses focus (leaving form field, etc.)
		
		+ Resets/Submit: form-specific events
		
		+ Mouse events: click, mouseover, drag, drop, etc.
	
	+ Other events:
		
		+ Media events: relate to audio/video playback
		
		+ Progress events: monitor ongoing browser progress
		
		+ CSS transition events: transition start/run/end
		
	+ Event reference: https://developer.mozilla.org/en-US/docs/Web/Events

##8. Project: Typing Speed Tester:

- event: keydown -> keypress -> keyup

- document.body.onkeydown=(e) => {console.log(e.key);}: print key pressed to console

##9. Loops

- Break: terminate the current loop, jump to the next stament

- Continue: terminate the current iteration of the loop, jump back up and run the next iteration.

##10. Project: Automated Responsive Images Markup

- Use sizes and srcset

- Reference: https://ericportis.com/posts/2014/srcset-sizes/

##11. Troubleshooting, Validating and Minifying Javascript

- Console debugging methods: http://developer.mozilla.org/en/docs/Web/API/console

- Script linting:

	+ Online
		
		+ JSLint
		
		+ JSHint
	
	+ Automated
		
		+ ESLint

- Minifying:

	+ Online: http://www.minifier.org/
	
	+ Automated: Uglify